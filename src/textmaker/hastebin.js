const easyFetch = (url, opts={}, params=null) => {
  const isForm = opts && ['POST', 'PUT', 'DELETE', 'PATCH'].indexOf(opts.method) > -1;
  const defaultOpts = {
    headers: (isForm ? {
      "Content-type": "application/json",
    } : {}),
  };
  defaultOpts.headers["Accept"] = "application/json";
  if (params) {
    const paramsString = Object.keys(params).map((k) => `${k}=${encodeURIComponent(params[k])}`).join('&');
    if (isForm) {
      defaultOpts['body'] = JSON.stringify(params);
    } else {
      url += "?" + paramsString;
    }
  }
  const finalOpts = {
    ...defaultOpts,
    ...opts,
  };
  return window
    .fetch(url, finalOpts)
    .then((response) => {
      if (response.status === 204) return {success: true};
      if (finalOpts.Accept === "application/json") {
        return response.json()
      } else {
        return response.text()
      }
    });
}

// doesn't work, sorry
const saveToHastebin = (data) => {
  return easyFetch('https://hastebin.com/documents', {method: "POST"}, {data});
}

const loadHastebin = (url) => {
  const id = url.startsWith("http") ? /\/([^/]+)(\.json)?$/g.exec(url)[1] : url.trim();
  const newURL = 'https://cors-anywhere.herokuapp.com/https://hastebin.com/raw/' + id;
  // const newURL = 'https://proxy-sauce.glitch.me/https://hastebin.com/raw/' + id;
  return easyFetch(newURL, {"Accept": "text/plain"});
}

export {
  saveToHastebin,
  loadHastebin,
}