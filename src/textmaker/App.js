import React, { Component } from 'react';

import { testScript, GooninatorScript } from "./model";
import TextPlayer from "./TextPlayer";
import ScriptEditor from "./ScriptEditor"

class App extends Component {
  constructor() {
    super();
    try {
      this.state = {script: new GooninatorScript(JSON.parse(localStorage.script))};
    } catch(e) {
      this.state = {script: testScript};
      console.log("new script");
    }
  }

  update() {
    this.setState(this.state);
  }

  render() {
    return (
      <div className="TextMaker">
        <div className="TextMaker__EditorContainer">
          <ScriptEditor
            script={this.state.script}
            onLoad={(s) => {
              this.setState({script: s});
            }}
            onSave={this.update.bind(this)} />
        </div>
        <div className="TextMaker__PlayerContainer">
          <TextPlayer script={this.state.script} isPlaying={true} />
        </div>
      </div>
    );
  }
}

export default App;
