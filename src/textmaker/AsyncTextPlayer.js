import React, { Component } from 'react';
import {loadHastebin} from "./hastebin";
import {GooninatorScript} from "./model";
import TextPlayer from "./TextPlayer";


// props {hastebinId, isPlaying}
export default class AsyncTextPlayer extends Component {
  constructor() {
    super();
    this.state = {error: null, script: null};
    this.lastHastebin = null;
  }

  render() {
    if (this.state.script) return (
      <TextPlayer script={this.state.script} isPlaying={this.props.isPlaying} />
    );
    if (this.error) return (
      <div classname="AsyncTextPlayer__error">{this.error}</div>
    );
    return (
      <div className="AsyncTextPlayer__loading"></div>
    );
  }
}