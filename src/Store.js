function encode(str) {
  return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
    return '%' + c.charCodeAt(0).toString(16);
  });
}

const location = window.location;
const history = window.history;


class Store {
    static URL_KEYS = [
        'timingFunction',
        'pastebinId',
        'youtubeId',
        'numConcurrentImages',
        'minimumImageWidth',
        'isRunning',
        'includeGifs',
        'includeStills',
        'zoom',
        'sources'
    ];

    static EPHEMERAL_KEYS = [
        "isSidebarOpen",
        "isTumblrIdeasOpen",
        "isHistoryOpen",
        "isFocusLost",
        "playHistory",
    ];

    urlKeys() {
        return Store.URL_KEYS;
    }

    constructor(namespace, initialValue, firstRun) {
        this.initialValue = initialValue;

        if (localStorage[namespace] && !location.search) {
            const storedState = JSON.parse(localStorage[namespace]);
            this.state = {
                ...initialValue,
                timingFunction: storedState.timingFunction,
                includeGifs: storedState.includeGifs,
                includeStills: storedState.includeStills,
                numConcurrentImages: storedState.numConcurrentImages,
                minimumImageWidth: storedState.minimumImageWidth,
                shouldShowHelpWhilePaused: storedState.shouldShowHelpWhilePaused,
                shouldPauseForLostFocus: storedState.shouldPauseForLostFocus,
                favorites: storedState.favorites,
                history: storedState.history,
            };
        } else {
            this.state = {...initialValue};
        }

        this._parseURL();

        Store.EPHEMERAL_KEYS.forEach((k) => {
            this.state[k] = initialValue[k];
        })

        this.subscribers = [];
        this.subscribe((newState) => console.info(newState));
        this.subscribe((newState) => {
            const savedState = {...this.state};
            Store.EPHEMERAL_KEYS.forEach((k) => {
                delete savedState[k];
            });
            localStorage["gn2"] = JSON.stringify(savedState);
        });
        this.subscribe((newState) => {
          let query = `sources=${encode(newState.sources.join(' '))}`;
          Store.URL_KEYS.forEach((k) => {
            if (k === 'sources') return;
            if (newState[k] !== initialValue[k]) {
              query += `&${k}=${encode("" + newState[k])}`;
            }
          });
          history.replaceState({}, document.title, `?${query}`)
        })
    }

    isValueDifferentFromDefault(k, v) {
      return v !== this.initialValue[k];
    }

    _parseURL() {
        if (!location.search) return;
        const search = location.search.substring(1);
        let params;
        try {
            params = JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
        } catch (e) {
            console.warn("Nonfatal URL parsing error:");
            console.error(e);
            return;
        }
        if (params.sources) {
            this.state.sources = params.sources
                .split(' ')
                .map((s) => s.trim())
                .filter((s) => s.length > 0);
        }
        [
            'includeGifs',
            'includeStills',
            'isRunning',
            'numConcurrentImages',
            'minimumImageWidth',
            'zoom',
        ].forEach((k) => {
            if (params[k]) this.state[k] = JSON.parse(params[k]);
        });
        [
            'timingFunction',
            'pastebinId',
            'youtubeId',
        ].forEach((k) => {
            if (params[k]) this.state[k] = params[k];
        });
    }

    subscribe = (fn) => {
        this.subscribers.push(fn);
    }

    setState = (newState) => {
        this.state = newState;
        this.subscribers.forEach((fn) => {
            try {
                fn(newState);
            } catch (e) {
                console.warn("Error while updating state:");
                console.error(e);
            }
        });
    }

    mutator = (fn, callback = null) => {
        return (e) => {
            if (e && e.preventDefault) {
                e.preventDefault();
                e.stopPropagation();
            }
            const newState = fn(this.state)
            this.setState(newState);
            if (callback) callback(newState);
        }
    }

    setter = (key, value) => {
        return this.mutator((oldState) => ({
            ...oldState,
            [key]: value,
        }));
    }

    updater = (values, callback = null) => {
        return this.mutator((oldState) => ({
          ...oldState,
          ...values,
        }), callback);
    }

    inputSetter = (key) => {
        return (e) => {
            this.setState({
                ...this.state,
                [key]: e.target.value,
            });
        };
    }

    numberSetter = (key) => {
        return (e) => {
            this.setState({
                ...this.state,
                [key]: parseInt(e.target.value, 10),
            });
        };
    }

    pusher = (key, value) => {
        return this.mutator((oldState) => {
          if (oldState[key].indexOf(value) !== -1) return oldState;
          return {
            ...oldState,
            [key]: oldState[key].concat([value]),
          }
        });
    }
}


const initialValue = {
    // url params
    sources: [],
    timingFunction: 'variableMedium',
    pastebinId: '',
    youtubeId: '',
    includeGifs: true,
    includeStills: true,
    zoom: null,
    numConcurrentImages: 5,
    minimumImageWidth: 500,
    isRunning: false,

    // "preferences"
    shouldShowHelpWhilePaused: true,
    shouldPauseForLostFocus: true,
    directoryCategoryIndex: null,
    favorites: {},
    history: [],

    // ephemeral state
    isSidebarOpen: true,
    isTumblrIdeasOpen: false,
    isHistoryOpen: false,
    isPastebinIdeasOpen: false,
    isFocusLost: false,
    playHistory: [],
};


const store = new Store("gn2", initialValue);

store.subscribe((state) => {
  if (!state.isRunning) return;
  const entry = {};
  store.urlKeys().forEach((k) => {
    entry[k] = state[k];
  });

  const isDifferent = state.history.length < 1 || function() {
    const lastEntry = state.history[state.history.length - 1];
    return JSON.stringify(entry) !== JSON.stringify(lastEntry);
  }();

  if (isDifferent) {
    store.pusher('history', entry)()
    const ga = window.ga; if (!ga) return;
    ga('send', {
      hitType: 'event',
      eventCategory: 'History',
      eventAction: 'append',
      eventLabel: JSON.stringify(entry),
    });
  };
});

export default store;