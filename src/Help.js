import React, { Component } from 'react';


export default class Help extends Component {
  static propTypes = {
  };

  render() {
    return (
      <div className="fill-container gn-help">
        <h1><img className="logo" src="/logo.png" alt="The Gooninator" /></h1>

        <p>
        This page will turn a list of tumblrs into a rapid-fire slideshow of
        stills that continually changes. While you could hypothetically use it to
        look at pictures of kittens really fast, it's more useful in, um, other
        ways.
        </p>

        <h1>Adding images</h1>

        <p>
        In the left sidebar, enter the name of a tumblr you want to see
        images of, and press Enter to add it to the list.
        </p>

        <p>
        To filter by a specific tag (new feature!) you can
        put a slash after it, and then the tag.
        Example: <tt>iwantmygflikethis/caption</tt>
        </p>

        <p>
        To see that blog's <i><strong>faves</strong></i>, use <tt>/faves</tt>.
        Example: <tt>rubberdolljenna/faves</tt>
        </p>

        <p>
        The “Tumblr Ideas” directory has some built-in tumblrs for you
        to use, but you can type in whatever you like.
        </p>

        <p>
          You can enter a bunch of tumblrs at once separated by spaces.
        </p>

        <p>
          After you enter some tumblrs, you can press 'tab' to unfocus the text field
          and then 'space' to start playing. No mouse required!
        </p>

        <h1>YouTube playlists</h1>
      
        <p>You can add a YouTube playlist for audio. Note I said PLAYLIST, not single video.</p>

        <p>Go to a YouTube playlist page. The URL will look like this:</p>

        <p><tt>https://www.youtube.com/watch?v=CevxZvSJLk8&amp;list=RDEM9E_ik5ScxhRL1c_HWNxA2A</tt></p>

        <p>
          You want the part between the first = and the &amp;, so in this case <tt>CevxZvSJLk8</tt>.
          Paste that into the "youtube playlist ID" field and have fun!
        </p>

        <h1>Favorites</h1>

        <p>
        Once a tumblr is in the list, you can click on it to favorite it.
        It will then show up in Tumblr Ideas.
        </p>

        <h1>Hastebin (text)</h1>

        <p>
        The Gooninator can display flashing text on top of the images. Text is
        hosted on <a href="https://hastebin.com">Hastebin</a> and follows a very specific format,
        described <a href="http://gooninator.neocities.org/text_help.html">here</a>.
        </p>

        <p>
        Enter your text in Hastebin, click Save, and copy the ID from the end of the URL
        and paste it into the "Hastebin ID" box.
        </p>

        <p>
        The text may take a few seconds to load, and the Hastebin proxy that
        the Gooninator uses sometimes goes down.
        </p>

        {/*
        <h1>Gooning History</h1>

        <p>
        Every time you press Play, a history entry is recorded with all your
        current settings. At any time, you can open the History view and click
        one to start playing it again.
        </p>
        */}

        <h1>Where is the data stored?</h1>

        <p>
        In your web browser. The Gooninator has no database. If you use private
        browsing mode, your data might not be saved.
        </p>

        <h1>Contributions, bug reports, and open source</h1>

        <p>
        Send bug reports and feedback to <a href="mailto:omgsekrit@yahoo.com">omgsekrit@yahoo.com</a>.
        </p>

        <p>
        Gooninator 2 is open source. You can contribute here: <a href="https://bitbucket.org/omgsekrit/gooninator">
        https://bitbucket.org/omgsekrit/gooninator2</a>
        </p>

        <h1>Thanks</h1>

        <p>Thanks to <a href="http://liv2gag.tumblr.com">liv2gag</a>
          , <a href="http://sissykuchi.tumblr.com">sissykuchi</a>
          , <a href="http://subbyfootslave.tumblr.com">subbyfootslave</a>
          , <a href="http://inannaworshiper.tumblr.com">inannaworshipper</a>
          , <a href="http://latrineus.tumblr.com">latrineus</a>
          , <a href="http://curiousissy999.tumblr.com">curiousissy999</a>
          , <a href="http://amiafagsissy.tumblr.com">amiafagsissy</a>
          , <a href="http://entrancedsluttypup.tumblr.com">entrancedsluttypup</a>
          , <a href="http://r4ws3w.tumblr.com">r4ws3w</a>
          , <a href="http://lockedinpanties.tumblr.com">lockedinpanties</a>
          , and <a href="http://pervtherion.tumblr.com">pervtherion</a> for contributing scripts!
        </p>

      </div>
    );
  }
}
