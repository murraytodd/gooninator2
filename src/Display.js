import React, {Component, PropTypes} from "react";
import Fetcher from "./Fetcher";
import Loader from "./Loader";
import CaptionProgram from "./CaptionProgram";
import ImageDisplay from "./display/ImageDisplay";
import timingFunctions from "./timingFunctions";
import store from "./Store";
import AsyncTextPlayer from "./textmaker/AsyncTextPlayer";


function mutateStoreToAddHistory(source, imageSrc) {
  return;  // this is too slow
  store.state.playHistory.push({source, imageSrc});
  if (store.state.playHistory.length > 100) store.state.playHistory.shift(); 
  store.setState(store.state);  // no-op, just fire update
}


export default class Display extends Component {

    static propTypes = {
        sources: PropTypes.arrayOf(PropTypes.string).isRequired,
        pastebinId: PropTypes.string,
    }

    constructor() {
        super();
        this.state = {queueLength: 0, image: null};
        this.fetchersBySource = {};
        this.loader = new Loader();
        this.timer = false;
    }

    componentDidMount() {
        this._update(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this._update(nextProps);
    }

    isRunning(props) {
      if (props.isFocusLost && props.shouldPauseForLostFocus) return false;
      return props.isRunning;
    }

    _update(props) {
        if (this.isRunning(props)) {
            this._run(props);
        } else {
            this._stop(props);
        }
    }

    _run(props) {
        props.sources.forEach((source) => {
            if (!this.fetchersBySource[source]) {
                this.fetchersBySource[source] = new Fetcher(source);
            }
        });
        this.loader.configure(
            props.sources.map((source) => this.fetchersBySource[source]),
            5,     // num concurrent
            120,     // max queue length
            props.includeGifs,  // gifs
            props.includeStills,  // stills
            props.minimumImageWidth,
            (queueLength) => {
                this.setState({queueLength});
            });
        this.loader.start();

        if (!this.timer) {
          this.timer = setTimeout(this.advanceAndReschedule, 0);
        }
    }

    _stop(props) {
      if (this.timer) {
        clearTimeout(this.timer);
        this.timer = null;
      }
      this.loader.stop();
    }

    advanceAndReschedule = () => {
      const {image, source} = this.loader.getNext();
      this.setState({image});

      if (source) {
        mutateStoreToAddHistory(source, image.src);
      }

      const timingFunction = timingFunctions[this.props.timingFunction];
      let t = 0;
      if (timingFunction) {
        t = timingFunction();
      } else {
        t = timingFunctions.variableMedium();
      }
      this.timer = setTimeout(this.advanceAndReschedule, t);
    };

    renderLoading() {
      const width = `${100 * (this.state.queueLength / 5)}%`;
      return (
        <div className="gn-display-loading">
          <div className="gn-display-loading-inner" style={{width}} />
        </div>
      );
    }

    render() {
        const isLoading = this.isRunning(this.props) && this.loader.queue.length < 5;
        return (
            <div className="gn-display fill-container" onClick={store.setter('isTumblrIdeasOpen', false)}>
                {isLoading && this.renderLoading()}
                {!isLoading && (
                  <ImageDisplay img={this.state.image} />)}
                {!isLoading && this.props.pastebinId && this.isRunning(this.props) && (
                  <CaptionProgram pastebinId={this.props.pastebinId} />)}
                {!isLoading && this.props.pastebinId && (
                  <AsyncTextPlayer hastebinId={this.props.pastebinId} isPlaying={true} />)}
            </div>
        );
    }
}