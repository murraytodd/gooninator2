/* utility functions */

function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);  // eslint-disable-line
      return v.toString(16);
    });
}

/* classes */

class Style {
    constructor(data = {}) {
        this.id = data.id || uuid();
        this.template = data.template || "";
        this.animationIn = data.animationIn || "";
        this.animationOut = data.animationOut || "";
        this.extraCSS = data.extraCSS || "";
        this.defaultArgs = {...data.defaultArgs};
    }

    getArgNames() {
        const names = [];
        const re = /\$(\w+)\b/g;
        var results;
        const stringToSearch = this.template + this.animationIn + this.animationOut + this.extraCSS;
        while ((results = re.exec(stringToSearch)) !== null) {  // eslint-disable-line
            names.push(results[1]);
        }
        return names.sort();
    }

    render(templateKey, args) {
        let result = this[templateKey];
        const allArgs = {...this.defaultArgs};
        Object.keys(args).forEach((k) => {
            if (args[k]) allArgs[k] = args[k];
        });
        Object.keys(allArgs).forEach((k) => {
            result = result.replace(`$${k}`, allArgs[k]);
        });
        return result;
    }
}

class Part {
    constructor(data = {styleId: null, strings: [], duration: 1}) {
        this.styleId = data.styleId;
        this.strings = data.strings;
        this.duration = data.duration || 1;
        this.stringTweenDelay = data.stringTweenDelay || 0;
        this.nextBehavior = data.nextBehavior || 'next';
        this.styleArgs = data.styleArgs || {};
        this.behavior = data.behavior || 'sequence';
    }
    
    isEmpty() {
        for (let s of this.strings) {
            if (s.length > 0) return false;
        }
        return true;
    }

    getDuration() {
        if (this.behavior === 'choice') return this.duration * 1000;
        let d = this.strings.length * this.duration;
        if (this.strings.length > 1) {
            d += (this.strings.length - 1) * this.stringTweenDelay;
        }
        return d * 1000;
    }

    // call after init, then pass to getString on each call
    getStoredString() {
        if (this.behavior === 'choice') {
            if (this.isEmpty()) return "";
            return this.strings[Math.floor(Math.random() * this.strings.length)];
        } else {
            return null;
        }
    }

    getString(t, storedString) {
        if (this.behavior === 'choice') {
            return storedString;
        }

        t /= 1000;
        let i = 0;
        while (true) {
            if (i === this.strings.length - 1) return this.strings[i];
            if (t < this.duration) { return this.strings[i]; }
            t -= this.duration;
            if (t < this.stringTweenDelay) { return ""; }
            i += 1;
        }
    }
}

class GooninatorScript {
    constructor(data) {
        this.styles = (data.styles || []).map(styleData => new Style(styleData));
        this.parts = (data.parts || []).map(partData => new Part(partData));
        this.name = data.name || '';
    }

    renderStyle(k, part) {
        const style = this.getStyle(part);
        if (!style) return "";
        return style.render(k, part.styleArgs);
    }

    getStyle(part) {
        return this.styles.filter(style => style.id === part.styleId)[0];
    }

    getNextPartIndex(partIndex) {
        const oldPart = this.parts[partIndex];
        if (oldPart.nextBehavior === 'next') {
            const newPartIndex = partIndex + 1;
            if (newPartIndex >= this.parts.length) {
                return 0;
            } else {
                return newPartIndex;
            }
        } else {
            return Math.floor(Math.random() * this.parts.length);
        }
    }
}


const testScript = new GooninatorScript({
    styles: [
        {
            id: 'default',
            template: `
                color: $color;
                font-size: $size;
            `,
            animationIn: "transition: opacity 2s ease-in-out;",
            animationOut: "opacity: 0; transition: opacity 2s ease-in-out;",
            extraCSS: "",
            defaultArgs: {
                color: 'white',
                size: '8vw',
            },
        },
    ],
    parts: [
        {
            styleId: 'default',
            strings: ["Beep", "beep"],
            duration: 3,
            behavior: 'choice',
            nextBehavior: 'next',
            styleArgs: {color: 'green', size: '12vw'},
        },
        {
            styleId: 'default',
            strings: ["Boop"],
            duration: 3,
            nextBehavior: 'next',
        },
        /*
        {
            styleId: 'default',
            strings: [""],
            duration: 3,
            nextBehavior: 'next',
        },
        */
    ],
});


export {
    Style,
    Part,
    GooninatorScript,
    testScript,
}