import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';


function go() {
    ReactDOM.render(<App />, document.getElementById('textmaker-root'));
    window.requestAnimationFrame(go);
};

// registerServiceWorker();

go();