import React, { Component } from 'react';
import { Style, Part } from "./model";

class StringList extends Component {
    onChange(i, e) {
        this.props.strings[i] = e.target.value;
        this.props.onChange(this.props.strings);
    }

    add(e) {
        e.preventDefault();
        this.props.strings.push("");
        this.props.onChange(this.props.strings);
        const self = this;
        setTimeout(() => {
            const inputs = self.el.querySelectorAll('input');
            inputs[inputs.length - 1].focus();
        }, 100);
    }

    remove(i, e) {
        e.preventDefault();
        this.props.strings.splice(i);
        this.props.onChange(this.props.strings);
    }

    render() {
        return (
            <div className="StringList" ref={(el) => { this.el = el; }}>
                {this.props.strings.map((s, i) => {
                    return <form className="StringList__String" key={i} onSubmit={this.add.bind(this)}>
                        <input value={s} onChange={this.onChange.bind(this, i)} />
                        <a onClick={this.remove.bind(this, i)}> &times;</a>
                    </form>
                })}
                <button onClick={this.add.bind(this)}>+ text</button>
            </div>
        );
    }
}

class DictEditor extends Component {
    onChange(k, e) {
        this.props.values[k] = e.target.value;
        this.props.onChange(this.props.values);
    }

    render() {
        return <div className="DictEditor">
            {this.props.keys.map(k => {
                return <div className="DictEditor__Row" key={k}>
                    <label>{k}: </label>
                    {" "}
                    <input
                        type="text"
                        value={this.props.values[k] || ""}
                        onChange={this.onChange.bind(this, k)} />
                </div>;
            })}
        </div>;
    }
}

class StyleEditor extends Component {
    constructor() {
        super();
        this.state = {text: "", id: ""};
    }

    onChange(k, e) {
        this.onChangeRaw(k, e.target.value);
    }

    onChangeRaw(k, val) {
        this.props.style[k] = val;
        this.props.onChange();
        this.setState({
            text: this.props.style.template,
            id: this.props.style.id,
        });
    }

    onChangeDefaults(k, e) {
        this.props.style.defaultArgs[k] = e.target.value;
        this.props.onChange();
        this.setState({
            // bleh, just trigger a state change
            text: this.props.style.template,
            id: this.props.style.id,
        });
    }

    delete() {
        if (confirm(`Are you sure you want to delete style "${this.props.style.id}"?`)) {  // eslint-disable-line
            this.props.onDelete();
        }
    }

    render() {
        const style = this.props.style;
        let text = "";
        for (let line of style.template.trim().split('\n')) {
            text += `${line.trim()}\n`;
        }
        text = style.template;
        return (
            <div className="StyleEditor">
                <h2>
                <label>name:</label>
                {" "}
                <input type="text" value={this.props.style.id} onChange={this.onChange.bind(this, 'id')} />
                </h2>

                <label>css:</label>
                <textarea value={text} onChange={this.onChange.bind(this, 'template')} />
                <label>default values:</label>
                <DictEditor
                    keys={style.getArgNames()}
                    values={this.props.style.defaultArgs}
                    onChange={this.onChangeRaw.bind(this, 'defaultArgs')} />
                <br />

                <label>animate in:</label><br />
                <textarea
                    type="text"
                    value={this.props.style.animationIn}
                    onChange={this.onChange.bind(this, 'animationIn')} />
                <br />
                <label>animate out:</label><br />
                <textarea
                    type="text"
                    value={this.props.style.animationOut}
                    onChange={this.onChange.bind(this, 'animationOut')} />
                <br />
                <label>extra css:</label><br />
                <textarea
                    type="text"
                    value={this.props.style.extraCSS}
                    onChange={this.onChange.bind(this, 'extraCSS')} />

                <br /><br />
                <button onClick={this.delete.bind(this)}>delete</button>
                {" "}
                <button onClick={this.props.onClone.bind(this)}>clone</button>
            </div>
        );
    }
}

class StylesEditor extends Component {
    constructor() {
        super();
        this.state = {n: 0};
    }

    onChange() {
        this.setState({n: this.state.n + 1});
    }

    add() {
        let nameN = 1
        while (this.props.script.getStyle(`style ${nameN}`)) {
            nameN += 1;
        }
        this.props.script.styles.push(new Style({
                id: `style ${nameN}`,
                template: `
                    color: $color;
                    font-size: $size;
                `,
                animationIn: "transition: opacity 0s ease-in-out;",
                animationOut: "opacity: 0; transition: opacity 0s ease-in-out;",
                extraCSS: "",
                defaultArgs: {
                    color: 'white',
                    size: '8vw',
                },
            }))
    }

    clone(i) {
        let nameN = 1
        while (this.props.script.getStyle(`style ${nameN}`)) {
            nameN += 1;
        }
        const data = JSON.parse(JSON.stringify(this.props.script.styles[i]));
        data.id = `style ${nameN}`;
        this.props.script.styles.push(new Style(data));
    }

    render() {
        return (
        <div className="StylesEditor">
            {this.props.script.styles.map((style, i) => {
                return <StyleEditor
                    key={i}
                    style={style}
                    onDelete={() => {
                        this.props.script.styles = this.props.script.styles.filter((s) => s !== style);
                        this.onChange();
                    }}
                    onClone={() => { this.clone(i); }}
                    onChange={this.onChange.bind(this)} />;
            })}
            <div className="ButtonContainer">
                <button onClick={this.add.bind(this)}>Add style</button>
            </div>
        </div>
        );
    }
}

class PartEditor extends Component {
    constructor() {
        super();
        this.state = {n: 0};
    }

    forceRender() {
        this.setState({n: this.state.n + 1});
        this.props.onChange();
    }

    onChange(k, e) {
        this.props.part[k] = e.target.value;
        this.forceRender();
    }

    onChangeRaw(k, value) {
        this.props.part[k] = value;
        this.forceRender();
    }

    onChangeNumber(k, e) {
        this.props.part[k] = parseFloat(e.target.value, 10);
        this.forceRender();
    }

    delete() {
        if (confirm(`Are you sure you want to delete this part?`)) {  // eslint-disable-line
            this.props.onDelete();
        }
    }

    /*
            styleArgs: {},
    */

    render() {
        const {part, style} = this.props;
        return (
            <div className="PartEditor">
                {this.props.part.strings.length < 1 && (
                    <div>(no text; just wait)</div>
                )}
                <StringList
                    strings={this.props.part.strings}
                    onChange={this.onChangeRaw.bind(this, 'strings')} />
                <br />

                <label>text order:</label>
                {" "}
                <select
                    value={this.props.part.behavior}
                    onChange={this.onChange.bind(this, 'behavior')}>
                    <option value="sequence">play all in order</option>
                    <option value="choice">choose one randomly</option>
                </select>

                <br />
                <label>style:</label>
                {" "}
                <select onChange={this.onChange.bind(this, 'styleId')} value={part.styleId}>
                    {this.props.styleIds.map(id => <option key={id} value={id}>{id}</option>)}
                </select>

                <br />
                <label>duration per text:</label>
                {" "}
                <input
                    className="SmallInput"
                    type="text"
                    value={this.props.part.duration}
                    onChange={this.onChangeNumber.bind(this, 'duration')} />

                {this.props.part.strings.length > 1 && (
                    <div>
                        <label>wait between each text:</label>
                        {" "}
                        <input
                            className="SmallInput"
                            type="text"
                            value={this.props.part.stringTweenDelay}
                            onChange={this.onChangeNumber.bind(this, 'stringTweenDelay')} />
                    </div>
                )}

                {this.props.style && (
                    <div>
                        <br />
                        <label>style args:</label>
                        <DictEditor
                            keys={style.getArgNames()}
                            values={part.styleArgs}
                            onChange={this.onChangeRaw.bind(this, 'styleArgs')} />
                    </div>
                )}

                <br />
                <label>after showing text:</label>
                {" "}
                <select
                    value={this.props.part.nextBehavior}
                    onChange={this.onChange.bind(this, 'nextBehavior')}>
                    <option value="next">go to next</option>
                    <option value="random">go to random part</option>
                </select>

                <br />
                <button className="pure-button" onClick={this.props.onMoveDown}>&darr;</button>
                {" "}
                <button className="pure-button" onClick={this.props.onMoveUp}>&uarr;</button>
                {" "}
                <button className="pure-button" onClick={this.delete.bind(this)}>delete</button>
            </div>
        );
    }
}

class PartsEditor extends Component {
    constructor() {
        super();
        this.state = {n: 0};
    }

    forceRender() {
        this.setState({n: this.state.n + 1});
    }

    add() {
        this.props.script.parts.push(new Part({
            styleId: 'default',
            strings: [],
            duration: 1,
            stringTweenDelay: 0,
            behavior: 'sequence',
            nextBehavior: 'next',
            styleArgs: {},
        }));
        this.forceRender();
    }

    render() {
        const {parts, styles} = this.props.script;
        return (
            <div className="PartsEditor">
                {this.props.script.parts.map((part, i) => {
                    return <PartEditor
                        key={i}
                        partIndex={i}
                        part={part}
                        styleIds={styles.map((style) => style.id)}
                        style={this.props.script.getStyle(part)}
                        onDelete={() => {
                            this.props.script.parts = parts.filter((p) => p !== part);
                            this.forceRender();
                        }}
                        onMoveDown={() => {
                            if (i >= parts.length - 1) { return; }
                            const swapWithPart = parts[i + 1];
                            parts[i + 1] = part;
                            parts[i] = swapWithPart;
                            this.forceRender();
                        }}
                        onMoveUp={() => {
                            if (i < 1) { return; }
                            const swapWithPart = parts[i - 1];
                            parts[i - 1] = part;
                            parts[i] = swapWithPart;
                            this.forceRender();
                        }}
                        onChange={this.forceRender.bind(this)} />;
                })}
                <div>
                    <button
                        className="pure-button pure-button-primary"
                        onClick={this.add.bind(this)}>
                        + Add part
                    </button>
                </div>
            </div>
        );
    }
}


export { PartsEditor, StylesEditor };