import React, { Component } from 'react';
import { PartsEditor, StylesEditor } from "./Editors";
import { GooninatorScript } from "./model";
import "./pure.css";
import {loadHastebin} from "./hastebin";

class HastebinText extends Component {
    render() {
        return (
            <div className="HastebinText">
                Paste this text to <a href="https://hastebin.com">hastebin.com</a>.
                {" "}
                (Unfortunately there is no way to do it automatically.)

                <br />
                <textarea
                    readOnly={true}
                    style={{width: '100%', height: '20rem'}}
                    value={JSON.stringify(this.props.script, null, "  ")} />
            </div>
        )
    }
}

// props: {tabs: [{label, render}]}
class TabControl extends Component {
    constructor() {
        super();
        this.state = {tabIndex: 0};
    }

    render() {
        return (
            <div className="TabControl">
                <div className="TabControl__Tabs pure-button-group" role="group">
                    {this.props.tabs.map(({label}, i) => {
                        let className = "TabControl__Tab pure-button";
                        if (i === this.state.tabIndex) {
                            className += " pure-button-primary";
                        }
                        return (
                            <div className={className}
                                 key={label}
                                 onClick={() => this.setState({tabIndex: i}) }>
                                {label}
                            </div>
                        )
                    })}
                </div>
                {this.props.tabs[this.state.tabIndex].render()}
            </div>
        );
    }
}

class ScriptEditor extends Component {
    constructor() {
        super();
        this.state = {n: 0, hastebinError: null};
    }

    save() {
        localStorage.script = JSON.stringify(this.props.script);
    }

    componentWillMount() {
        const doSave = () => {
            this.save()
            setTimeout(doSave, 1);
        }
        doSave();
    }

    onChangeName(e) {
        this.props.script.name = e.target.value;
        this.setState({n: this.state.n + 1});
    }

    loadHastebin(e) {
        e.preventDefault();
        this.setState({hastebinError: null});
        loadHastebin(this.hastebinEl.value)
            .then((response) => {
                try {
                    const newScript = new GooninatorScript(JSON.parse(response))
                    this.props.onLoad(newScript);
                    console.log("Loaded", response, newScript);
                } catch (e) {
                    this.setState({hastebinError: e});
                    console.log(response);
                    throw e;
                }
            });
    }

    render() {
        return (
        <div className="ScriptEditor">
            <form className="ScriptEditor__Form pure-form pure-form-stacked" onSubmit={this.loadHastebin.bind(this)}>
                <label htmlFor="hastebin">
                    Paste a hastebin URL here and hit Enter to load it:
                    <input id="hastebin" ref={(el) => { this.hastebinEl = el; }} />
                </label>

                <br />
                {this.state.hastebinError && (
                    <div style={{color: 'red'}}>Error loading hastebin</div>
                )}
            </form>
            <form className="ScriptEditor__Form pure-form pure-form-stacked">
                <label htmlFor="name">
                    Script name:
                    <input
                        id="name"
                        value={this.props.script.name}
                        onChange={this.onChangeName.bind(this)} />
                </label>

                <br />
            </form>
            <div onClick={this.props.onToggleIsPlaying}>
                <a>{this.props.isPlaying ? "pause" : "play"}</a>
            </div>
            <br />
            <br />
            <TabControl
                tabs={[
                    {
                        label: "Parts",
                        render: () => <PartsEditor script={this.props.script} />,
                    },
                    {
                        label: "Styles",
                        render: () => <StylesEditor script={this.props.script} />,
                    },
                    {
                        label: "Hastebin",
                        render: () => <HastebinText script={this.props.script} />
                    },
                ]}
                />
        </div>
        );
    }
}

export default ScriptEditor;