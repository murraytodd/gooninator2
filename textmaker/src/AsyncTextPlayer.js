import React, { Component } from 'react';
import {loadHastebin} from "./hastebin";
import {GooninatorScript} from "./model";
import TextPlayer from "./TextPlayer";


// props {hastebinId, isPlaying}
export default class AsyncTextPlayer extends Component {
  constructor() {
    super();
    this.state = {error: null, script: null};
    this.lastHastebin = null;
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props);
  }

  componentWillReceiveProps(props) {
    if (props.hastebinId == this.lastHastebin) return;
    this.lastHastebin = props.hastebinId;
    loadHastebin(props.hastebinId)
      .then((text) => {
        this.setState({
          error: null,
          script: new GooninatorScript(JSON.parse(text)),
        });
      })
      .catch((e) => {
        this.setState({
          error: e,
          script: null,
        });
      });
  }

  render() {
    if (this.state.script) return (
      <TextPlayer script={this.state.script} isPlaying={this.props.isPlaying} />
    );
    if (this.error) return (
      <div classname="AsyncTextPlayer__error">{this.error}</div>
    );
    return (
      <div className="AsyncTextPlayer__loading">Loading...</div>
    );
  }
}