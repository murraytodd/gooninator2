import React, { Component } from 'react';
import './TextPlayer.css';

function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);  // eslint-disable-line
      return v.toString(16);
    });
}


// props: {script, t}
class TextPlayer extends Component {
    constructor() {
        super()
        this.state = {
            // {part, index, style, tStarted, storedString, state='queued'|'running'|'exiting', exitTime}
            onscreenParts: [],
        }
        this.renderNum = 0;
        this.isPaused = false;
        this.timeSpentPaused = 0;
        this.lastPauseStart = null;
    }

    componentWillReceiveProps(props) {
        if (props.isPlaying && this.isPaused) {
            this.timeSpentPaused += Date.now() - this.lastPauseStart;
            this.isPaused = false;
            return;
        }
        if (!props.playing && !this.isPaused) {
            this.lastPauseStart = Date.now();
            this.isPaused = true;
            return;
        }
    }

    now() {
        return Date.now() - this.timeSpentPaused;
    }

    componentWillMount() {
        this.setState({onscreenParts: [this.addPart(0, false)]});
    }

    updateState(newState = null) {
        const next = this.state.onscreenParts.filter(({state}) => state !== 'dead')
        if (newState) {
            next.push(newState);
        }
        setTimeout(() => {
            this.setState({onscreenParts: next});
        }, 0);
    }

    advance() {
        if (!this.state.onscreenParts.length) {
            return [this.addPart(0)];
        }

        let willUpdate = false;
        const statesToRender = [];
        this.state.onscreenParts.forEach((partState, i) => {
            const {part, index, tStarted, state, exitTime, renderNum} = partState;
            const dt = this.now() - tStarted;

            if (state === 'exiting' && this.now() >= exitTime) {
                willUpdate = true;
                partState.state = 'dead';
                return
            }

            if (state === 'queued' && this.renderNum > renderNum) {
                willUpdate = true;
                partState.state = 'running';
                statesToRender.push(partState);
                return;
            }

            if (state === 'running' && dt > part.getDuration()) {
                partState.state = 'exiting';
                partState.exitTime = this.now() + 2000;
                statesToRender.push(partState);
                const nextIndex = this.props.script.getNextPartIndex(index)
                statesToRender.push(this.addPart(nextIndex));
                willUpdate = false;
                return
            }

            statesToRender.push(partState);
        });

        if (willUpdate) {
            this.updateState();
        }

        return statesToRender;
    }

    addPart(index, update = true) {
        const part = this.props.script.parts[index];
        const newState = {
            key: uuid(),
            part,
            index,
            style: this.props.script.getStyle(part),
            tStarted: this.now(),
            storedString: part.getStoredString(),
            state: 'queued',
            exitTime: null,
            renderNum: this.renderNum + 2,
        };
        if (update) {
            this.updateState(newState);
        }
        return newState;
    }

    render() {
        this.renderNum += 1;
        if (this.props.script.parts.length < 1) {
            return <div className="TextPlayer" />;
        }

        let parts = [];
        if (this.props.isPlaying) {
            parts = this.advance();
        } else {
            parts = this.state.onscreenParts;
        }

        return (
            <div className="TextPlayer">
                {parts.map(({part, index, style, key}) => {
                    if (!style) return null;
                    return (
                        <style key={`style-${key}`} dangerouslySetInnerHTML={{__html: `
                            .TextPlayer__Text.TextPlayer__Text-specific-${key} {
                                ${style.render('template', part.styleArgs)}
                            }
                            .TextPlayer__Text.TextPlayer__Text-specific-${key}.state-queued {
                                opacity: 0;
                                transform: scale(1.0);
                            }
                            .TextPlayer__Text.TextPlayer__Text-specific-${key}.state-running {
                                ${style.render('animationIn', part.styleArgs)}
                            }
                            .TextPlayer__Text.TextPlayer__Text-specific-${key}.state-exiting {
                                ${style.render('animationOut', part.styleArgs)}
                            }
                            ${style.render('extraCSS', part.styleArgs)}
                        `}} />
                    );
                })}
                {parts.map(({part, index, style, storedString, key, tStarted, state}) => {
                    const dt = this.now() - tStarted;
                    return <div className="TextPlayer__TextContainer" key={key}>
                        <div
                            className={`TextPlayer__Text TextPlayer__Text-specific-${key} state-${state}`}>
                            {part.getString(dt, storedString)}
                        </div>
                    </div>
                })}
            </div>
        );
    }
}


export default TextPlayer;