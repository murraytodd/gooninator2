import React, { Component } from 'react';

import { testScript, GooninatorScript } from "./model";
import TextPlayer from "./TextPlayer";
import ScriptEditor from "./ScriptEditor"

class App extends Component {
  constructor() {
    super();
    try {
      this.state = {
        script: new GooninatorScript(JSON.parse(localStorage.script)),
        isPlaying: true,
      };
    } catch(e) {
      this.state = {script: testScript, isPlaying: true};
      console.log("new script");
    }
    this.setState({isPlaying: true});
  }

  update() {
    this.setState(this.state);
  }

  render() {
    return (
      <div className="TextMaker">
        <div className="TextMaker__EditorContainer">
          <ScriptEditor
            isPlaying={this.state.isPlaying}
            script={this.state.script}
            onToggleIsPlaying={() => this.setState({isPlaying: !this.state.isPlaying})}
            onLoad={(s) => { this.setState({script: s}); }}
            onSave={this.update.bind(this)} />
        </div>
        <div className="TextMaker__PlayerContainer">
          <TextPlayer script={this.state.script} isPlaying={this.state.isPlaying} />
        </div>
      </div>
    );
  }
}

export default App;
